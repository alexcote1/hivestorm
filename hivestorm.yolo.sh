#!/bin/bash
#Section 1.1 - Checks for root
if [ $(whoami) = "root" ]; then
  echo "You are root"
else
  echo "You are not root"
  exit 1
fi
#Section 1.2 - Checks for folders
if [ ! -d /files ]; then
  mkdir /files
fi
echo " Making the quarantine media files folder"
if [ -d /quarantine/ ]; then
  echo "/quarantine/ folder is already made"
else
  mkdir /quarantine/
fi
if [ -d /quarantine/mediafiles ]; then
  echo "/quarantine/mediafiles folder is already made"
else
  mkdir /quarantine/mediafiles
fi
#Section 2.1 - Creates a text file called "mediafiles1" and gives it the neccasary permissions
touch /files/mediafiles1
chmod u+rwx /files/mediafiles1
#Section 2.2 - Finds any files within any home folders then sends their names to the mediafiles1 text file, each file on a new line
find /home -iname "*.pot" -print  >> /files/mediafiles1
find /home -iname "*.rhost" -print  >> /files/mediafiles1
find /home -iname "*.rec" -print  >> /files/mediafiles1
#Section 2.3 - Loops through each line in mediasfiles1, then moves each file to a quarantine folder
echo "Done with media files"
echo
#Section 3.1 - Opens a nano prompt where the user types in the names of every actual user on the system, seperated by a new line for every username
#Section 4.1 - Asks the user if they are supposed to be an SSH server
echo "Moving on to securing SSH"
echo -n "Is this machine supposed to be an SSH server [y/n]"
read ssh
if [ $ssh == y ]; then
  echo "Fixing SSH files"
  apt-get install -y ssh openssh-client openssh-server
  if [ -a ]; then
    sed -i.bak '/PasswordAuthentication/c\PasswordAuthentication yes' /etc/ssh/sshd_config
    sed -i.bak '/Protocol/c\Protocol 2' /etc/ssh/sshd_config
    sed -i.bak '/PermitRootLogin/c\PermitRootLogin no' /etc/ssh/sshd_config
    sed -i.bak '/X11Forwarding/c\X11Forwarding no' /etc/ssh/sshd_config
    sed -i.bak '/UsePam/c\UsePam yes' /etc/ssh/sshd_config
    sed -i.bak '/RSAAuthentication/c\RSAAuthentication yes' /etc/ssh/sshd_config
    sed -i.bak '/PermitEmptyPasswords/c\PermitEmptyPasswords no' /etc/ssh/sshd_config
    sed -i.bak '/LoginGraceTime/c\LoginGraceTime 60' /etc/ssh/sshd_config
    sed -i.bak '/StrictModes/c\StrictModes yes' /etc/ssh/sshd_config
    sed -i.bak '/IgnoreRhosts/c\IgnoreRhosts yes' /etc/ssh/sshd_config
    sed -i.bak '/TCPKeepAlive/c\TCPKeepAlive no' /etc/ssh/sshd_config
    sed -i.bak '/UsePrivilegeSeperation/c\UsePrivilegeSeperation yes' /etc/ssh/sshd_config
    sed -i.bak '/PubkeyAuthentication/c\PubkeyAuthentication yes' /etc/ssh/sshd_config
    sed -i.bak '/PermitBlacklistedKeys/c\PermitBlacklistedKeys no' /etc/ssh/sshd_config
    sed -i.bak '/HostbasedAuthentication/c\HostbasedAuthentication no' /etc/ssh/sshd_config
    sed -i.bak '/PrintMotd/c\PrintMotd no' /etc/ssh/sshd_config
  fi
  update-rc.d ssh defaults
  update-rc.d ssh enable
  iptables -A INPUT -p tcp --dport ssh -j ACCEPT
  service ssh restart
  service ssh reload
  echo "Finished fixing SSH files"
fi
if [ $ssh == n ]; then
  echo "Removing and blocking SSH"
  apt-get purge -ymqq ssh openssh-client openssh-server
  service ssh stop
  update-rc.d -f ssh remove
  iptables -A INPUT -p tcp --dport ssh -j DROP
  iptables -A INPUT -p tcp --dport 22 -j DROP
  sudo /sbin/iptables-save > /etc/iptables.save
  sudo /sbin/service iptables save
  sudo /etc/init.d/iptables save
  echo "Finished removing and blocking SSH"
fi
echo "SSH security done"
#Section 4.2 - Asks the user if they are supposed to be an Apache server
echo "Moving on to securing Apache"
echo -n "Is this machine supposed to be an Apache server [y/n]"
read apache
if [ $apache == y ]; then
  echo "Fixing Apache files"
  apt-get install -ymqq --allow-unauthenticated apache2 libapache2-modsecurity
  chown -R root:root /etc/apache2
  chown -R root:root /etc/apache
  update-rc.d apache2 defaults
  update-rc.d apache2 enable
  #chmod u+rwx /var/www/html/
  #chmod g+rx /var/www/html/ && chmod g-w /var/www/html/
  #chmod o+rx /var/www/html/ && chmod o-w /var/www/html/
  sed -i.bak '/Timeout/c\Timeout 45' /etc/apache2/apache2.conf
  sed -i.bak '/KeepAlive/c\KeepAlive Off' /etc/apache2/apache2.conf
  sed -i.bak '/MaxKeepAliveRequests/c\MaxKeepAliveRequests 50' /etc/apache2/apache2.conf
  sed -i.bak '/KeepAliveTimeout/c\KeepAliveTimeout 1' /etc/apache2/apache2.conf
  sed -i.bak '/HostnameLookups/c\HostnameLookups Off' /etc/apache2/apache2.conf

  if [ -f "/etc/apache2/conf.d/security" ]; then
    sed -i.bak '/ServerTokens/c\ServerTokens Prod' /etc/apache2/conf.d/security
    sed -i.bak '/ServerSignature/c\ServerSignature Off' /etc/apache2/conf.d/security
    sed -i.bak '/TraceEnable/c\TraceEnable Off' /etc/apache2/conf.d/security
  else
    sed -i.bak '/ServerTokens/c\ServerTokens Prod' /etc/apache2/conf-enabled/security.conf
    sed -i.bak '/ServerSignature/c\ServerSignature Off' /etc/apache2/conf-enabled/security.conf
    sed -i.bak '/TraceEnable/c\TraceEnable Off' /etc/apache2/conf-enabled/security.conf
  fi
  
  cat /etc/apache2/apache2.conf | grep "LimitRequestBody 204800" >> /dev/null
  if [ $? -ne 0 ]; then
#      echo \<Directory \> >> /etc/apache2/apache2.conf
#      echo -e ' \t AllowOverride None' >> /etc/apache2/apache2.conf
#      echo -e ' \t Order Deny,Allow' >> /etc/apache2/apache2.conf
#      echo -e ' \t Deny from all' >> /etc/apache2/apache2.conf
#      echo -e ' \t Options None' >> /etc/apache2/apache2.conf
#      echo \<Directory \/\> >> /etc/apache2/apache2.conf
#	echo UserDir disabled root >> /etc/apache2/apache2.conf
    echo LimitRequestBody 204800 >> /etc/apache2/apache2.conf
  fi
  a2dismod autoindex
  a2dismod status
  sed -i.bak '/SecRuleEngine.*/c\SecRuleEngine On' /etc/apache2/mods-enabled/security2.conf
  mv /etc/modsecurity/modsecurity.conf-recommended /etc/modsecurity/modsecurity.conf
  mv /etc/apache2/mods-available/ssl.load /etc/apache2/mods-enabled/
  service apache2 restart
  iptables -A INPUT -p tcp --dport 80 -j ACCEPT
  iptables -A INPUT -p tcp --dport 443 -j ACCEPT
  service apache2 restart
  ls -la etc/apache2/sites-enabled
  echo "Finished fixing Apache files"
fi
if [ $apache == n ]; then
  echo "Removing Apache"
  apt-get purge -ymqq --allow-unauthenticated apache2 libapache2-modsecurity
  service apache2 stop
  update-rc.d -f apache2 remove
  iptables -A INPUT -p tcp --dport 80 -j DROP
  iptables -A INPUT -p tcp --dport 443 -j DROP
  echo "Finished removing Apache"
fi
echo "Apache security done"
#Section 4.3 - Asks the user if they are supposed to be an Samba server
echo "Moving on to securing Samba"
echo -n "Is this machine supposed to be an Samba server [y/n]"
read samba
if [ $samba == y ]; then
  echo "Fixing Samba files"
  apt-get install -ymqq --allow-unauthenticated samba smbclient libsmbclient system-config-samba
  cat /etc/samba/smb.conf | grep usershare allow guests | grep yes
  if [ $?==0 ]; then
    sed -i.bak '/usershare allow guests/c\usershare allow guests no' /etc/samba/smb.conf
	
    msg=$(echo usershare allow guests rule changed | sed 's/\//%2F/g' | sed 's/\./%2E/g' | sed 's/\ /%20/g' )
  fi
  update-rc.d samba defaults
  update-rc.d samba enable
  update-rc.d smbd defaults
  update-rc.d smbd enable
  update-rc.d nmbd defaults
  update-rc.d nmbd enable
  ufw allow samba
  iptables -A INPUT -p tcp --dport 445 -j ACCEPT
  iptables -A INPUT -p tcp --dport 137 -j ACCEPT
  iptables -A INPUT -p tcp --dport 138 -j ACCEPT
  iptables -A INPUT -p tcp --dport 139 -j ACCEPT
  service samba restart
  service smbd restart
  service nmbd restart
  echo "Finished fixing Samba files"
fi
if [ $samba == n ]; then
  echo "Removing and blocking Samba"
  apt-get purge -ymqq --allow-unauthenticated samba smbclient libsmbclient
  service samba stop
  service smbd stop
  service nmbd stop
  update-rc.d -f samba remove
  update-rc.d -f smbd remove
  update-rc.d -f nmbd remove
  iptables -A INPUT -p tcp --dport 137 -j DROP
  iptables -A INPUT -p tcp --dport 138 -j DROP
  iptables -A INPUT -p tcp --dport 139 -j DROP
  iptables -A INPUT -p tcp --dport 445 -j DROP
  echo "Finished removing and blocking Samba"
fi
echo "Samba security done"
#Section 4.4 - Asks the user if they are supposed to be an FTP server
echo "Moving on to securing FTP"
echo -n "Is this machine supposed to be an FTP server [y/n]"
read ftp
if [ $ftp == y ]; then
  echo "Fixing FTP files"
  apt-get install -ymqq --allow-unauthenticated vsftpd
  if [ -e /etc/vsftpd.conf ]; then
    cat /etc/vsftpd.conf | grep anonymous_enable | grep yes
    if [ $?==0 ]; then
      sed -i.bak '/anonymous_enable/c\anonymous_enable no' /etc/vsftpd.conf
      msg=$(echo anonymous_enable rule changed | sed 's/\//%2F/g' | sed 's/\./%2E/g' | sed 's/\ /%20/g' )
    fi
    cat /etc/vsftpd.conf | grep write_enable | grep yes
    if [ $?==0 ]; then
      sed -i.bak '/write_enable/c\write_enable no' /etc/vsftpd.conf
      msg=$(echo write_enable rule changed | sed 's/\//%2F/g' | sed 's/\./%2E/g' | sed 's/\ /%20/g' )
    fi
    cat /etc/vsftpd.conf | grep anon_upload_enable | grep yes
    if [ $?==0 ]; then
      sed -i.bak '/anon_upload_enable/c\anon_upload_enable no' /etc/vsftpd.conf
      msg=$(echo anon_upload_enable rule changed | sed 's/\//%2F/g' | sed 's/\./%2E/g' | sed 's/\ /%20/g' )
    fi
  fi
  
  #Start of proftpd
  
  #Check for anon

  cat /etc/proftpd/proftpd.conf | grep '</Anonymous>' | grep '#' 2>/dev/null
  if [ $? -ne 0 ]; then
    echo "You need to comment out anonymous login in /etc/proftpd/proftpd.conf"
  fi

  #Turn of IdentLookups

  sed -i.bak '/IdentLookups/c\IdentLookups off' /etc/proftpd/proftpd.conf

  #Disable all WRITE commands to every share under /

  echo "<Directory />" >> /etc/proftpd/proftpd.conf
  echo "   <Limit WRITE>" >> /etc/proftpd/proftpd.conf
  echo "      DenyAll" >> /etc/proftpd/proftpd.conf
  echo "   </Limit>" >> /etc/proftpd/proftpd.conf
  echo "</Directory>" >> /etc/proftpd/proftpd.conf
  
  ufw allow ftp
  ufw reload
  service vsftpd restart
  service proftpd restart
  echo "Finished fixing FTP files"
fi
if [ $ftp == n ]; then
  echo "Removing and blocking FTP"
  apt-get purge -ymqq --allow-unauthenticated vsftpd* proftpd*
  service vsftpd stop
  service proftpd stop
  ufw deny ftp
  ufw reload
  echo "Finished removing and blocking FTP"
fi
echo "FTP security done"
#Section 4.5 - Asks the user if they are supposed to be an MySQL server
echo "Moving on to securing MySQL"
echo -n "Is this machine supposed to be an MySQL server (Type nothing if you have or need a third party MySQL server program ex. MariaDB) [y/n]"
read mysql
if [ $mysql == y ]; then
  apt-get install -ymqq --allow-unauthenticated mysql-client mysql-server
  update-rc.d mysql defaults
  update-rc.d mysql enable
  ufw allow mysql
  ufw reload
  service mysql restart
  echo "Finished fixing MySQL files"
fi
if [ $mysql == n ]; then
  echo "Removing and blocking MySQL"
  apt-get purge -ymqq --allow-unauthenticated mysql-client mysql-server mariadb-client mariadb-server
  service mysql stop
  update-rc.d -f mysql remove
  ufw deny mysql
  ufw reload
  echo "Finished removing and blocking MySQL"
fi
echo "MySQL security done"
#Section 4.6 - Asks the user if they are supposed to be an DNS server
echo "Moving on to securing DNS"
echo -n "Is this machine supposed to be an DNS server [y/n]"
read dns
if [ $dns == y ]; then
  echo "Fixing DNS files"
  apt-get install -ymqq --allow-unauthenticated dnsutils bind9
  update-rc.d bind9 defaults
  update-rc.d bind9 enable
  ufw allow bind9
  ufw reload
  service bind9 restart
  echo "Finished fixing DNS files"
fi
if [ $dns == n ]; then
  echo "Removing and blocking DNS"
  apt-get purge -ymqq --allow-unauthenticated bind9
  service bind9 stop
  update-rc.d -f bind9 remove
  ufw deny bind9
  ufw reload
  echo "Finished removing and blocking DNS"
fi
echo "DNS security done"
#Section 5.1 - Sets up Auditing
echo "Doing Auditing Now"
auditctl -e 1 > /dev/null
sed -i.bak '/num_logs/c\num_logs = 4' /etc/audit/auditd.conf
sed -i.bak '/max_log_file/c\max_log_file = 5' /etc/audit/auditd.conf
echo "auditing done"
echo
#Section 5.2 - Sets up Login.defs
echo "Securing Login.defs now"
sed -i.bak '/LOG_OK_LOGINS.*/c\LOG_OK_LOGINS           yes' /etc/login.defs
sed -i.bak '/PASS_MIN_DAYS.*/c\PASS_MIN_DAYS   10' /etc/login.defs
sed -i.bak '/PASS_MAX_DAYS.*/c\PASS_MAX_DAYS   90' /etc/login.defs
sed -i.bak '/LOGIN_RETRIES.*/c\LOGIN_RETRIES           3' /etc/login.defs
sed -i.bak '/PASS_WARN_AGE.*/c\PASS_WARN_AGE   7' /etc/login.defs
sed -i.bak '/ENCRYPT_METHOD.*/c\ENCRYPT_METHOD SHA512' /etc/login.defs
echo
#Section 5.3 - Sets up Pam
echo "Securing Pam.d now"
apt-get install -ymqq --allow-unauthenticated libpam-cracklib
grep "auth 	required 			pam_tally.so deny=5 onerr=fail unlock_time=1800 audit even_deny_root_account silent" /etc/pam.d/common-auth >> /dev/null
if [ "$?" -eq "1" ]; then
  echo "auth 	required 			pam_tally.so deny=5 onerr=fail unlock_time=1800 audit even_deny_root_account silent" >> /etc/pam.d/common-auth
fi
grep "pam_cracklib.so retry=3 minlen=8 difok=3 ucredit=-1 lcredit=-1 dcredit=-1 ocredit=-1" /etc/pam.d/common-password >> /dev/null
if [ "$?" -eq "1" ]; then
  sed -i.bak '/password	requisite			pam_cracklib.so.*/c\password	requisite			pam_cracklib.so retry=3 minlen=8 difok=3 ucredit=-1 lcredit=-1 dcredit=-1 ocredit=-1' /etc/pam.d/common-password
  sed -i.bak '/password	[success=1 default=ignore]	pam_unix.so.*/c\password	[success=1 default=ignore]	pam_unix.so obscure use_authtok try_first_pass sha512 minlen=8 remember=5' /etc/pam.d/common-password
fi
echo
echo "Editing Sysctl.conf now"
sed -i.bak '/net.ipv4.ip_forward.*/c\net.ipv4.ip_forward=0' /etc/sysctl.conf
sed -i.bak '/net.ipv4.tcp_syncookies.*/c\net.ipv4.tcp_syncookies=1' /etc/sysctl.conf
sysctl -w net.ipv4.tcp_syncookies=1
sysctl -w net.ipv4.ip_forward=0
sysctl -w net.ipv4.conf.all.send_redirects=0
sysctl -w net.ipv4.conf.default.send_redirects=0
sysctl -w net.ipv4.conf.all.accept_redirects=0
sysctl -w net.ipv4.conf.default.accept_redirects=0
sysctl -w net.ipv4.conf.all.secure_redirects=0
sysctl -w net.ipv4.conf.default.secure_redirects=0
sysctl -p
echo
#Section 5.4 - Removes any users from sudoers file that do not require a password
#echo "Editing Sudoers file now"
grep NOPASSWD /etc/sudoers

#Making the user saftey, adding to sudo group, setting password, making home directory

useradd saftey  
usermod -aG sudo saftey
echo -n "saftey:SecurePassword" | chpasswd
mkdir /home/saftey

#Adding saftey to the sudoers file

echo "safety  ALL=(ALL:ALL) ALL" >> /etc/sudoers

#Comenting out all lines with "NOPASSWD" that are not already commented

sed -e '/NOPASSWD/ s/^#*/#/' -i /etc/sudoers

echo "Sudoers file is done"
echo
#Section 5.5 - Sets up LightDM
echo "Moving on to securing LightDM"
echo -n "Is this machine running Ubuntu 12.04 or 14.04 [12/14]"
read lightdm
if [ $lightdm == 12 ]; then
  echo "Fixing LightDM files for Ubuntu 12.04"
  /usr/lib/lightdm/lightdm-set-defaults -l false
  if [ $?==0 ]; then
    msg=$(echo Set allow guest to false | sed 's/\//%2F/g' | sed 's/\./%2E/g' | sed 's/\ /%20/g' )
  fi
  echo "exit 0" > /etc/rc.local
  msg=$(echo X11Forwarding rule changed to exclusively 1 | sed 's/\//%2F/g' | sed 's/\./%2E/g' )
  grep "[SeatDefaults]" /etc/lightdm/lightdm.conf
  if [ "$?" -eq "1" ]; then
    echo "[SeatDefaults]" >> /etc/lightdm/lightdm.conf
  fi  
  grep "greeter-hide-users=true" /etc/lightdm/lightdm.conf
  if [ "$?" -eq "1" ]; then
    echo "greeter-hide-users=true" >> /etc/lightdm/lightdm.conf
  fi
  grep "greeter-show-manual-login=true" /etc/lightdm/lightdm.conf
  if [ "$?" -eq "1" ]; then
    echo "greeter-show-manual-login=true" >> /etc/lightdm/lightdm.conf
  fi
  echo "Finished fixing LightDM files for Ubuntu 12.04"
fi
if [ $lightdm == 14 ]; then
  echo "Fixing LightDM files for Ubuntu 14.04"
  if [ ! -d /etc/lightdm/lightdm.conf.d ]; then
    mkdir -p /etc/lightdm/lightdm.conf.d
  fi
  if [ -d /etc/lightdm/lightdm.conf.d ]; then
    if [ ! -f /etc/lightdm/lightdm.conf.d/50-guest-session.conf ]; then
    echo "[SeatDefaults]" > /etc/lightdm/lightdm.conf.d/50-guest-session.conf
    echo "allow-guest=false" >> /etc/lightdm/lightdm.conf.d/50-guest-session.conf
    echo "[SeatDefaults]" > /etc/lightdm/lightdm.conf.d/50-show-users.conf
    echo "greeter-hide-users=true" >> /etc/lightdm/lightdm.conf.d/50-show-users.conf
    echo "[SeatDefaults]" > /etc/lightdm/lightdm.conf.d/50-manual-login.conf
    echo "greeter-show-manual-login=true" >> /etc/lightdm/lightdm.conf.d/50-manual-login.conf
    fi
  fi
  echo "Finished fixing LightDM files for Ubuntu 14.04"
fi
echo "Remember to restart LightDM (or the system) when the script is done"
echo "LightDM security done"
#Section 6.1 - Removes any common hacking tools
echo "Uninstalling packages now"
apt-get purge --auto-remove john hydra aircrack kismet medusa nmap vlc fingerd finger netcat dhcpdump snort p0f aircrack-ng nc hydra-gtk bind9 wireshark
echo
#Section 6.2 - Sets up file permissions
echo "Changing file permissions now"
chmod 0700 /etc/hosts.allow
chmod 0700 /etc/mtab
chmod 0700 /etc/utmp
chmod 0700 /var/adm/wtmp
chmod 0700 /var/log/wtmp
chmod 0700 /etc/syslog.pid
chmod 0700 /var/run/syslog.pid
chmod 0700 /etc/sysctl.conf
chmod 0700 /etc/inittab
chmod 644 /etc/fstab

chmod 02750 /bin/su
chmod 02750 /bin/sudo
chmod 02750 /bin/ping
chmod 02750 /sbin/ifconfig
chmod 02750 /usr/bin/w
chmod 02750 /usr/bin/who
chmod 02750 /usr/bin/locate
chmod 02750 /usr/bin/whereis

rm -f /etc/cron.deny
rm -f /etc/at.deny
echo root > /etc/cron.allow
echo root > /etc/at.allow
chmod 400 /etc/cron.allow
chmod 400 /etc/at.allow
chown root:root /etc/cron.allow
chown root:root /etc/at.allow

#echo "Looking for all the files on the system with 777 permissions"
#find / -type d -perm +2 -ls -exec grep -vi "dev|tmp" {} +


iptables --policy INPUT DROP
sudo /sbin/iptables-save > /etc/iptables.save
sudo /sbin/service iptables save
sudo /etc/init.d/iptables save
echo
#Section 7.2 - Clears hosts file
echo "Clearing host file now"
cp /etc/hosts /etc/backuphosts
echo 127.0.0.1	localhost > /etc/hosts
echo 127.0.1.1	$(hostname)  >> /etc/hosts
echo ::1     ip6-localhost ip6-loopback >> /etc/hosts
echo fe00::0 ip6-localnet >> /etc/hosts
echo ff00::0 ip6-mcastprefix >> /etc/hosts
echo ff02::1 ip6-allnodes >> /etc/hosts
echo ff02::2 ip6-allrouters >> /etc/hosts
echo
echo "The host file is cleared"
#MP3 File Part of Script:

mkdir /quarantine
find / -iname "*.mp3" -type f -exec /bin/mv {} {}.bak \;
find / -iname "*.mp4" -type f -exec /bin/mv {} {}.bak \;

apt-get autoremove
apt-get autoclean

ufw enable
ufw logging high
ufw default deny incoming
